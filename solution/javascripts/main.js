(function(){
	//better to use modernizer btw
	/**
	 * Pre
	 * 
	 * Ensure that the device has touch capability
	 */
	function pre(){
		if("ontouchstart" in $("canvas")[0]){
			$("h1").html("Cool we have touch events on this device");
			init(true);
		}else{
			$("h1").html("We don't have touch events on this device, use the mouse to click");
			init(false);	
		}
	}
	
	/**
	 * Drop Pin
	 *
	 * Place a marker at said coordinates
	 * Depending upon multi touch support you might be able to press two fingers at the same time
	 * and create red dots instead of blue
	 *
	 * @param {Canvas 2d Context} ctx
	 * @param {Number} x
	 * @param {Number} y
	 * @param {Bool} multi //multiple fingers initiated this event
	 * @return {Undefined}
	 */
	function dropPin(ctx, x, y, multi){
		ctx.fillStyle = multi? "red" : "blue";
		ctx.fillRect(x,y,10,10);
	}
	
	/**
	 * Handle Mouse Start
	 *
	 * As we need to fallback to Desktop devices 
	 */
	function handleMouseStart(me){
		var multi = false,
			ctx = $("canvas")[0].getContext("2d"),
			origin_x = this.offsetLeft,
			origin_y = this.offsetTop,
			x = me.clientX - origin_x,
			y = me.clientY -origin_y;
		//check for shift key depresion
		//multi = !!shift_key_check;
		dropPin(ctx, x, y, multi);
	}
	
	/**
	 * Handle Touch Start
	 *
	 * This should recoginze multi touch taps that occur at the same time.
	 */
	function handleTouchStart(tse){
		tse.preventDefault();
		var html = '',
			i = 0,
			$output = $("output"),
			x = 0,
			y = 0,
			origin_x = this.offsetLeft,
			origin_y = this.offsetTop,
			count_fingers = 0,
			ctx = $("canvas")[0].getContext("2d"),
			multi = tse.changedTouches.length > 1 ? true : false;
		for(; i<tse.changedTouches.length; i+=1){
			count_fingers += 1;
			if(count_fingers > 1){
				html += '<br />...and we have a second finger tapping at the same time with these coordinates: ';
			}
			x = tse.changedTouches[i].pageX - origin_x;
			y = tse.changedTouches[i].pageY - origin_y;
			html +=  x + ', ' + y;
			dropPin(ctx, x, y, multi);
		}
		$output.html("I just touched these coordinates: " + html);	
	}
	
	/**
	 * Init
	 * 
	 * param {Bool} touchy //touch enabled
	 * Set up some event handlers
	 */
	function init(touchy){
		var $output = $("output"),
			$canvas = $("canvas");
		$output.html("Ready to collect touch events...");
		touchy ? 
			$canvas[0].addEventListener("touchstart", handleTouchStart, false) : 
			$canvas[0].addEventListener("click", handleMouseStart, false);
	}
	
	//and run
	pre();
})();